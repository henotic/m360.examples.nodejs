# m360.examples.nodejs

The M360 Nodejs Examples contains examples of how to use the M360 Middleware with your NodeJs Microservices & APIs.
The M360 Middleware is developed using native Nodejs and acts as a dependency that gets consumed by a NodeJs Server.
The examples include running the Middleware with [Express](https://expressjs.com/), [Fastify](https://www.fastify.io/), and [NestJs](https://nestjs.com/).


### Installation ###

---

Download the M30 Middleware from NPM and install its dependencies using the following commands:
```bash
$ npm install m360.mw.nodejs
```


### Usage ###

---

```javascript
//example using express
const server = require('express')();
const path = require('path');
const M360Mw = require("m360.mw.nodejs");

//add the M360 middleware
server.use(M360Mw({
    'contract': path.normalize(path.join(__dirname, 'contract.json')),
    'platform': 'manual',
    'ip': "127.0.0.1",
    'type': 'express',
    'server': server
}));

//using the middleware
server.get('/settings', function (request, reply) {
	
	//the middleware is auto attached to the request
	request.M360.registry.get(null, (error, registry) => {
		if (error) {
			return reply.send(error);
		} else {
			
			let custom = request.M360.custom.get();
			return reply.send({
				"registry": registry,
				"custom": custom
			});
		}
	});
});

// Run the server!
server.listen(4002, '0.0.0.0', function (err, address) {
	if (err) {
		throw err;
	}
	console.log(`server listening on 0.0.0.0:4002`);
});
```

The Middleware includes examples on how you can consume it with these servers.

These examples are located inside the **frameworks** folder in this repository.

Reference: [M360 Middleware Official Documentation](https://corsairm360.atlassian.net/wiki/spaces/Docs/pages/643923991/SDK)