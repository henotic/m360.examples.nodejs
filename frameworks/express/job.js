/*
 * Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

"use strict";
const path = require( 'path' );

//require the M360 Middleware SDK
const M360Mw = require( 'm360.mw.nodejs/lib/index');

//initialize the gateway connector and provide the name and version of the service that
//this job is helping
M360Mw.initJob({
				   'ip': '127.0.0.1',
	               'platform': 'manual',
	               'contract': path.normalize( path.join( __dirname, 'contract.json' ) ),
               }, (error, response) => {
	if ( error ) {
		throw error;
		process.exit( -1 );
	}
	
	//execute a function from the middleware ...
	M360Mw.registry.get( null, ( error, registry ) => {
		if ( error ) {
			throw error;
			process.exit( -1 );
		}
		
		M360Mw.logger.info( registry );
		process.exit( 0 );
	} );
});