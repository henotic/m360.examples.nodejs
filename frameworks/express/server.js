/*
 * Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

"use strict";

if (!process.env.APP_SSL_KEY || !process.env.APP_SSL_CERT) {
	console.error("\nError: Missing Application Private SSL certificate/key pair.");
	console.error("Please provide the paths to both the SSL certificate and its key either using environment variables ('APP_SSL_CERT' and 'APP_SSL_KEY')");
	console.info(`\n#########################################################################################`);
	console.info(`\nIf you don't have a certificate/key pair, you can use openssl to generate one.`);
	console.info(`Example:\n> openssl req -new -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout /user/privatekey.key -out /user/certificate.crt`);
	console.info(`\nReference: https://www.baeldung.com/openssl-self-signed-cert`);
	console.info(`\n#########################################################################################\n`);
	process.exit(-1);
}

const fs = require("fs");
const https = require("https");
const server = require('express')();
const path = require('path');
const utils = require('../utils/utils');

//add the M360 middleware
const M360Mw = require("m360.mw.nodejs");
const contractObj = require(path.normalize(path.join(__dirname, 'contract.json')));
contractObj.host = {
	"ssl": true,
	"weight": 1
};
server.use(M360Mw({
	'contract': contractObj,
	'ip': process.env.APP_IP || "127.0.0.1",
	'type': 'express',
	'server': server,
	'platform': 'manual'
	/**
	 * Provide the 4 options below when the microservice that consumes this middleware is intended to run in a kubernetes deployment
	 * @platform {String} value equals 'kubernetes'
	 * @namespace {String} value equals the kubernetes namespace where your deployment will run
	 * @service {String} value equals the name of the kubernetes service that is attached to the your deployment
	 * @exposedPort {String} value equals the exposed port of your kubernetes service
	 * @example
	 *      'platform': 'kubernetes',
	 *      'platformOptions": {
					                'namespace': 'mike',
					                'service': 'service-express',
					                'exposedPort': process.env.APP_PORT || 4002,
					            }
	 */
	
	/**
	 * Provide the 4 options below when the microservice that consumes this middleware is intended to run in a docker service
	 * @platform {String} value equals 'docker'
	 * @network {String} value equals the docker network attached to your docker service
	 * @service {String} value equals the name of the docker service
	 * @example
	 *      'platform': 'docker',
	 *      'platformOptions": {
					                'network': 'mike',
					                'service': 'service-express',
					            }
	 */
}));

server.get('/settings', function (request, reply) {
	utils.settings(request.M360, (error, data) => {
		if (error) {
			return reply.send(error);
		}
		return reply.send(data);
	});
});

server.get('/databases', function (request, reply) {
	return reply.send(utils.databases(request.M360, request));
});

server.get('/resources', function (request, reply) {
	return reply.send(utils.resources(request.M360));
});

server.get('/users', function (request, reply) {
	utils.users(request.M360, (error, users) => {
		if (error) {
			return reply.send(error);
		}
		
		return reply.send(users);
	});
});

server.get('/user/one', function (request, reply) {
	utils.oneUser(request.M360, '5f23b5fff05042751d6fea9d', (error, oneUser) => {
		if (error) {
			return reply.send(error);
		}
		
		return reply.send(oneUser);
	});
});

server.get('/request/user', function (request, reply) {
	return reply.send(utils.reqUser(request.M360, request));
});

server.get('/request/tenants', function (request, reply) {
	return reply.send(utils.reqTenants(request.M360, request));
});

server.post('/tenants', function (request, reply) {
	utils.dbTenants(request.M360, request.body.codes, (error, tenants) => {
		if (error) {
			return reply.send(error);
		}
		return reply.send(tenants);
	});
});

server.get('/awareness', function (request, reply) {
	utils.awareness(request.M360, (error, awareness) => {
		if (error) {
			return reply.send(error);
		}
		
		return reply.send(awareness);
	});
});

server.get('/awareness/discovery', function (request, reply) {
	utils.nextHost(request.M360, 'fastify', 1, (error, serviceHostInfo) => {
		if (error) {
			return reply.send(error);
		}
		
		return reply.send(serviceHostInfo);
	});
});

server.get('/proxy', function (request, reply) {
	utils.proxy(request.M360, {
		user: '5f23b5fff05042751d6fea9d',
		service: 'fastify',
		version: 1,
		route: '/settings',
		method: 'get'
	}, (error, proxyResponse) => {
		if (error) {
			return reply.send(error);
		}
		
		return reply.send(proxyResponse);
	});
});

server.get('/json', function (request, reply) {
	return reply.send(contractObj);
});

// Run the server!
https
	.createServer({
		key: fs.readFileSync(process.env.APP_SSL_KEY),
		cert: fs.readFileSync(process.env.APP_SSL_CERT)
	}, server)
	.listen(process.env.APP_PORT || 4002, process.env.APP_IP || '0.0.0.0', function (err, address) {
		if (err) {
			throw err;
		}
		console.log(`server listening on 0.0.0.0:4002`);
	});