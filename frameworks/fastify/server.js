/*
 * Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

"use strict";
if (!process.env.APP_SSL_KEY || !process.env.APP_SSL_CERT) {
	console.error("\nError: Missing Application Private SSL certificate/key pair.");
	console.error("Please provide the paths to both the SSL certificate and its key either using environment variables ('APP_SSL_CERT' and 'APP_SSL_KEY')");
	console.info(`\n#########################################################################################`);
	console.info(`\nIf you don't have a certificate/key pair, you can use openssl to generate one.`);
	console.info(`Example:\n> openssl req -new -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout /user/privatekey.key -out /user/certificate.crt`);
	console.info(`\nReference: https://www.baeldung.com/openssl-self-signed-cert`);
	console.info(`\n#########################################################################################\n`);
	process.exit(-1);
}

const fs = require('fs');
const path = require('path');
const utils = require('../utils/utils');
//add the M360 Middleware
const M360Mw = require("m360.mw.nodejs");

const contractObj = require(path.normalize(path.join(__dirname, 'contract.json')));

let ssl = {
	allowHTTP1: true, // fallback support for HTTP1
	key: fs.readFileSync(process.env.APP_SSL_KEY),
	cert: fs.readFileSync(process.env.APP_SSL_CERT)
};

const server = require('fastify')({
	https: ssl,
	logger: {
		level: 'debug',
		prettyPrint: true
	}
});

contractObj.host = {
	"ssl": true,
	"weight": 1
};

server.register(require('fastify-express'))
	.then(() => {
		const options = {
			'contract': contractObj, // path
			'ip': process.env.APP_IP || "127.0.0.1",
			'type': 'fastify', // fastify or express
			'server': server,
			'platform': process.env.APP_PLATFORM || 'manual',
			/**
			 * Provide the 4 options below when the microservice that consumes this middleware is intended to run in a kubernetes deployment
			 * @platform {String} value equals 'kubernetes'
			 * @namespace {String} value equals the kubernetes namespace where your deployment will run
			 * @service {String} value equals the name of the kubernetes service that is attached to the your deployment
			 * @exposedPort {String} value equals the exposed port of your kubernetes service
			 * @example
			 *      'platform': 'kubernetes',
			 *      'platformOptions": {
						               'namespace': 'mike',
						                'service': 'service-express',
						                'exposedPort': process.env.APP_PORT || 4002,
					                }
			 */
			
			/**
			 * Provide the 4 options below when the microservice that consumes this middleware is intended to run in a docker service
			 * @platform {String} value equals 'docker'
			 * @network {String} value equals the docker network attached to your docker service
			 * @service {String} value equals the name of the docker service
			 * @example
			 *      'platform': 'docker',
			 *      'platformOptions": {
							            'network': 'mike',
							            'service': 'service-express',
						            }
			 */
			
		};
		
		server.use(M360Mw(options));
		
		server.get('/settings', function (request, reply) {
			return new Promise((resolve, reject) => {
				utils.settings(request.raw.M360, (error, data) => {
					if (error) {
						return reject(error);
					}
					return resolve(data);
				});
			});
		});
		
		server.get('/databases', function (request, reply) {
			return new Promise((resolve) => {
				return resolve(utils.databases(request.raw.M360, request));
			});
		});
		
		server.get('/resources', function (request, reply) {
			return new Promise((resolve) => {
				return resolve(utils.resources(request.raw.M360));
			});
		});
		
		server.get('/users', function (request, reply) {
			return new Promise((resolve, reject) => {
				utils.users(request.raw.M360, (error, users) => {
					if (error) {
						return reject(error);
					}
					
					return resolve(users);
				});
			});
		});
		
		server.get('/user/one', function (request, reply) {
			return new Promise((resolve, reject) => {
				utils.oneUser(request.raw.M360, '5f23b5fff05042751d6fea9d', (error, oneUser) => {
					if (error) {
						return reject(error);
					}
					
					return resolve(oneUser);
				});
			});
		});
		
		server.get('/request/user', function (request, reply) {
			return new Promise((resolve) => {
				return resolve(utils.reqUser(request.raw.M360, request));
			});
		});
		
		server.get('/request/tenants', function (request, reply) {
			return new Promise((resolve) => {
				return resolve(utils.reqTenants(request.raw.M360, request));
			});
		});
		
		server.post('/tenants', function (request, reply) {
			return new Promise((resolve) => {
				utils.dbTenants(request.raw.M360, request.body.codes, (error, tenants) => {
					if (error) {
						return reject(error);
					}
					
					return resolve(tenants);
				});
			});
		});
		
		server.get('/awareness', function (request, reply) {
			return new Promise((resolve, reject) => {
				utils.awareness(request.raw.M360, (error, awareness) => {
					if (error) {
						return reject(error);
					}
					
					return resolve(awareness);
				});
			});
		});
		
		server.get('/awareness/discovery', function (request, reply) {
			return new Promise((resolve, reject) => {
				utils.nextHost(request.raw.M360, 'fastify', 1, (error, serviceHostInfo) => {
					if (error) {
						return reject(error);
					}
					
					return resolve(serviceHostInfo);
				});
			});
		});
		
		server.get('/proxy', function (request, reply) {
			return new Promise((resolve, reject) => {
				utils.proxy(request.raw.M360, {
					user: '5f23b5fff05042751d6fea9d',
					service: 'fastify',
					version: 1,
					route: '/settings',
					method: 'get'
				}, (error, proxyResponse) => {
					if (error) {
						return reject(error);
					}
					
					return resolve(proxyResponse);
				});
			});
		});
		
		server.get('/json', function (request, reply) {
			return new Promise((resolve) => {
				return resolve(require(path.normalize(path.join(__dirname, 'contract.json'))));
			});
		});
		
		let dataPort = process.env.APP_PORT || contractObj.ports.data;
		let appURL = process.env.APP_IP || '0.0.0.0';
		// Run the server!
		server.listen(dataPort, appURL, (err, address) => {
			if (err) {
				throw err;
			}
			console.log(`server listening on 0.0.0.0:${dataPort}`);
		});
		
	}).catch(error => {
	throw error;
});