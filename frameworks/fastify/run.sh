#!/usr/bin/env bash

#
# Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
#  Unauthorized copying of this file, via any medium is strictly prohibited.
#  Proprietary and confidential.
#

# set the gateway information, if you are running your gateway on a different port, add here
#export GATEWAY_MAINTENANCE_IP="127.0.0.1"

export GATEWAY_MAINTENANCE_IP="localhost"
export GATEWAY_MAINTENANCE_PORT="5000"

# set the data ip
# export APP_IP="localhost"

# set the data port to 4001
# export APP_PORT=4001

# ssl information,
# TODO: add here
#export APP_SSL_KEY=%path_to_ssl_cert_key%.key
#export APP_SSL_CERT=%path_to_ssl_cert_crt%.crt

export GATEWAY_MAINTENANCE_SSL=true

# start the application
node server.js