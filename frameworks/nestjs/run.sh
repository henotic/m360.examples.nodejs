#!/usr/bin/env bash

#
# Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
#  Unauthorized copying of this file, via any medium is strictly prohibited.
#  Proprietary and confidential.
#

# set the gateway information
export GATEWAY_MAINTENANCE_IP="127.0.0.1"
export GATEWAY_MAINTENANCE_PORT="5000"

# set the data ip
export APP_IP="localhost"

# set the data port to 4003
export APP_PORT=4003

# ssl information
#export APP_SSL_KEY=%path_to_ssl_cert_key%.key
#export APP_SSL_CERT=%path_to_ssl_cert_crt%.crt

export GATEWAY_MAINTENANCE_SSL=true


export APP_SSL_CERT=/opt/user/certificate.crt
export APP_SSL_KEY=/opt/user/privatekey.key


# start the application
npm run nestjs-server