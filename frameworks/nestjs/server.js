/*
 * Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

import {Controller, Get, Post, Module, Req, Res, Bind} from "@nestjs/common"
import {NestFactory} from '@nestjs/core';
import M360Mw from "m360.mw.nodejs";
import path from 'path';
import utils from '../utils/utils';
import fs from 'fs';

@Controller()
class AppController {
	
	@Get("/settings")
	@Bind(Req(), Res())
	settings(request, response) {
		utils.settings(request.M360, (error, data) => {
			if (error) {
				response.status(error.code || 172).send(error);
			}
			response.status(200).send(data);
		});
	}
	
	@Get("/databases")
	@Bind(Req())
	databases(request) {
		return utils.databases(request.M360, request);
	}
	
	@Get("/resources")
	@Bind(Req())
	resources(request) {
		return utils.resources(request.M360);
	}
	
	@Get("/users")
	@Bind(Req(), Res())
	users(request, response) {
		utils.users(request.M360, (error, users) => {
			if (error) {
				response.status(error.code || 172).send(error);
			}
			
			response.status(200).send(users);
		});
	}
	
	@Get("/user/one")
	@Bind(Req(), Res())
	userOne(request, response) {
		utils.oneUser(request.M360, '5f23b5fff05042751d6fea9d', (error, oneUser) => {
			if (error) {
				response.status(error.code || 172).send(error);
			}
			
			response.status(200).send(oneUser);
		});
	}
	
	@Get("/request/user")
	@Bind(Req())
	requestUser(request) {
		return utils.reqUser(request.M360, request);
	}
	
	@Get("/request/tenants")
	@Bind(Req())
	requestTenants(request) {
		return utils.reqTenants(request.M360, request);
	}
	
	@Post("/tenants")
	@Bind(Req())
	requestTenants(request) {
		utils.dbTenants(request.M360, request.body.codes, (error, tenants) => {
			if (error) {
				response.status(error.code || 172).send(error);
			}
			response.status(200).send(tenants);
		});
	}
	
	@Get("/awareness")
	@Bind(Req(), Res())
	awareness(request, response) {
		utils.awareness(request.M360, (error, awareness) => {
			if (error) {
				response.status(error.code || 172).send(error);
			}
			
			response.status(200).send(awareness);
		});
	}
	
	@Get("/awareness/discovery")
	@Bind(Req(), Res())
	awarenessDiscovery(request, response) {
		utils.nextHost(request.M360, 'fastify', 1, (error, serviceHostInfo) => {
			if (error) {
				response.status(error.code || 172).send(error);
			}
			
			response.status(200).send(serviceHostInfo);
		});
	}
	
	@Get("/proxy")
	@Bind(Req(), Res())
	proxy(request, response) {
		utils.proxy(request.M360, {
			user: '5f23b5fff05042751d6fea9d',
			service: 'fastify',
			version: 1,
			route: '/settings',
			method: 'get'
		}, (error, proxyResponse) => {
			if (error) {
				response.status(error.code || 172).send(error);
			}
			
			response.status(200).send(proxyResponse);
		});
	}
	
	@Get("/json")
	@Bind(Req())
	json(request) {
		return require(path.normalize(path.join(__dirname, 'contract.json')));
	}
}

@Module({
	imports: [],
	controllers: [AppController],
	providers: [],
})
class AppModule {
}

async function bootstrap() {
	const httpsOptions = {
		key: fs.readFileSync(process.env.APP_SSL_KEY),
		cert: fs.readFileSync(process.env.APP_SSL_CERT),
	};
	
	const app = await NestFactory.create(AppModule, {
		httpsOptions
	});
	const contractObj = require(path.normalize(path.join(__dirname, 'contract.json')));
	contractObj.host = {
		"ssl": true,
		"weight": 1
	};
	
	app.use(M360Mw({
		'contract': contractObj,
		'ip': process.env.APP_IP || "127.0.0.1",
		'type': 'nestjs',
		'server': app,
		'platform': 'manual',
		/**
		 * Provide the 4 options below when the microservice that consumes this middleware is intended to run in a kubernetes deployment
		 * @platform {String} value equals 'kubernetes'
		 * @namespace {String} value equals the kubernetes namespace where your deployment will run
		 * @service {String} value equals the name of the kubernetes service that is attached to the your deployment
		 * @exposedPort {String} value equals the exposed port of your kubernetes service
		 * @example
		 *      'platform': 'kubernetes',
		 *      'platformOptions": {
                    'namespace': 'mike',
                    'service': 'service-express',
                    'exposedPort': process.env.APP_PORT || 4002,
                }
		 */
		
		/**
		 * Provide the 4 options below when the microservice that consumes this middleware is intended to run in a docker service
		 * @platform {String} value equals 'docker'
		 * @network {String} value equals the docker network attached to your docker service
		 * @service {String} value equals the name of the docker service
		 * @example
		 *      'platform': 'docker',
		 *      'platformOptions": {
                     'network': 'mike',
                     'service': 'service-express',
                }
		 */
	}));
	
	await app.listen(process.env.APP_PORT || 4003, process.env.APP_IP || '0.0.0.0');
}

if (!process.env.APP_SSL_KEY || !process.env.APP_SSL_CERT) {
	console.error("\nError: Missing Application Private SSL certificate/key pair.");
	console.error("Please provide the paths to both the SSL certificate and its key either using environment variables ('APP_SSL_CERT' and 'APP_SSL_KEY')");
	console.info(`\n#########################################################################################`);
	console.info(`\nIf you don't have a certificate/key pair, you can use openssl to generate one.`);
	console.info(`Example:\n> openssl req -new -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout /user/privatekey.key -out /user/certificate.crt`);
	console.info(`\nReference: https://www.baeldung.com/openssl-self-signed-cert`);
	console.info(`\n#########################################################################################\n`);
	process.exit(-1);
}

bootstrap();



