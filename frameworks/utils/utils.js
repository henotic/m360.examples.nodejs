/*
 * Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

"use strict";

const Utils = {

    settings: (M360, cb) => {
        M360.registry.get(null, (error, registry) => {
            if (error) {
                return cb(error);
            } else {
                let custom = M360.custom.get();

                return cb(null, {
                    "registry": registry,
                    "custom": custom
                });
            }
        });
    },

    databases: (M360, request) => {
        let tenants = M360.tenant.list(request);
        let singleDBs = M360.database.get('single');
        let mtDBs = (tenants && tenants.length > 0) ? M360.database.get('multitenant', null, tenants[0].code) : null;

        return {
            'single': singleDBs,
            'multitenant': mtDBs
        }
    },

    resources: (M360) => {
        return M360.resource.get();
    },

    reqUser: (M360, request) => {
        return M360.user.get(request);
    },

    reqTenants: (M360, request) => {
        return M360.tenant.list(request);
    },
    
    dbTenants: (M360, codes, cb) => {
        M360.tenant.find(codes, cb);
    },

    users: (M360, cb) => {
        M360.user.find({}, cb);
    },

    oneUser: (M360, id, cb) => {
        M360.user.find({id: id}, cb);
    },

    awareness: (M360, cb) => {
        M360.awareness.get(null, null, null, cb);
    },

    nextHost: (M360, service, version, cb) => {
        M360.awareness.getNextHost(service, version, null, null, cb);
    },

    proxy: (M360, context, cb) => {
        M360.awareness.proxy(context, cb);
    },
};

module.exports = Utils;